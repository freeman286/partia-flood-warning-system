# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

# Initilise testing (Importing modules + building station list)
from floodsystem.station import *
from floodsystem.stationdata import build_station_list
stations = build_station_list()


def test_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s.latest_level = 1.893485
    
    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    
    assert s.typical_range_consistent() == True
    assert s.relative_water_level() == 0.73
    print("Test MonitoringStation Object: Successful")

def test_inconsistent_typical_range_stations():
    inconsistent = inconsistent_typical_range_stations(stations)
    for station in inconsistent:
        assert station.typical_range_consistent() == False
    print("Test inconsistent_typical_range_stations: Successful")

def test_consistent_typical_range_stations():
    consistent = consistent_typical_range_stations(stations)
    for station in consistent:
        assert station.typical_range_consistent() == True
    print("Test consistent_typical_range_stations: Successful")

test_monitoring_station()
test_inconsistent_typical_range_stations()
test_consistent_typical_range_stations()



















