# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius
from floodsystem.utils import stations_to_names


def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    x = stations_within_radius(stations, (52.2053, 0.1218), 10)
    x = list(stations_to_names(x)) # Sort alphabetically

    # Print stations within 10 km of Cambridge
    print(x)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()
