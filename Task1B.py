# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance


def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    x = stations_by_distance(stations, (52.2053, 0.1218))

    #Sorts and tabularise data
    x = list(map(lambda z: (z[0].name, z[0].town, z[1]), x))

    # Print the 10 closest and the 10 furthest station
    print("Data format:(Name,Town,Distance)")
    print()
    print("Closest 10 stations:",x[:10])
    print()
    print("Furthest 10 stations:",x[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()
