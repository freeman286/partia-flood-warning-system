from floodsystem.analysis import *

def polyfit(dates,levels,p):
    return (np.poly1d(np.polyfit(dates,levels,p)),max(dates)-min(dates))

def test_polyfit():
    p1,dt1 = polyfit([1,2,3,4,5],[0.1,1.6,8.1,25.6,62.5],4)
    p2,dt2 = polyfit([1,1.3,1.7,2,4.5],[5.7,7.1121,9.5269,11.8,52.8625],3)
    p1ans = [0.1,0,0,0,0]
    p2ans = [0.3,0.7,1.9,2.8]
    
    for i in range(len(p1.c)):
        assert round(p1.c[i],1) == p1ans[i]
    for i in range(len(p2.c)):
        assert round(p2.c[i],1) == p2ans[i]
	
    print("Test polyfit: Succesful")

test_polyfit()

