import datetime

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    """Requirements for Task 2F"""
    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    #Arbitary variables for task (displayN = 5)
    displayN = 5
    dt = 2

    stations = stations_highest_rel_level(stations, displayN)

    for station in stations:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_levels(station, dates, levels,4)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
