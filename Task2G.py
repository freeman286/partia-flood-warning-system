import datetime

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level, town_risk_level, stations_level_over_threshold


def run():
    """Requirements for Task 2G"""
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    stations = [i[0] for i in stations_level_over_threshold(stations, 0.8)]

    towns = town_risk_level(stations, 20, 3, 60)
    for k, v in towns.items():
        print("{} is at {} risk of flooding".format(k, v))

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
