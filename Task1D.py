# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.utils import stations_to_names


def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    data = stations_by_river(stations)
    rivers = {k: v for k, v in data.items() if len(v) > 0}.keys()

    # Print first 10 rivers with at least one monitoring station
    print(sorted(rivers)[:10])

    x = stations_by_river(stations)
    # Print stations on various rivers
    print(stations_to_names(x["River Aire"]))
    print(stations_to_names(x["River Cam"]))
    print(stations_to_names(x["River Thames"]))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
