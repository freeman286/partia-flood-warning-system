# Initilise testing (Importing modules + building station list)
from floodsystem.geo import *
from floodsystem.stationdata import build_station_list
stations = build_station_list()

#Task 1B
def test_stations_by_distance():
    #Initialise relavant lists/variables
    p = (52.2053, 0.1218)
    stations_dist = stations_by_distance(stations,p)

    #Test distance value exists for all stations
    for i in stations_dist:
        assert(i[1])
    
    #Test distance is correctly sorted
    for i in range(len(stations_dist)-1):
        assert(stations_dist[i][1] <= stations_dist[i+1][1])

    print("Test stations by distance: Succesful")

#Task 1C
def test_stations_within_radius():
    #Initialise relavant lists/variables
    p = (52.2053, 0.1218)
    r = 10
    stations_radius = stations_within_radius(stations, p, r)
    stations_dist = stations_by_distance(stations,p)


    #Test all stations_radius in list have distance < r
    for station in stations_dist:
        if station in stations_radius:
            assert(station[2]<r)

    print("Test stations within radius: Succesful")

#Task 1D    
def test_river_with_station():
    #Initialise relavant lists/variables
    rivers = rivers_with_station(stations)

    #Create list all station.river from all station objects
    stationriver_list = [station.river for station in stations]

    #Test if river exists in stationriver_list
    for r in rivers:
        assert (r in stationriver_list)

    print("Test river with station: Succesful")

def test_stations_by_river():
    #Initialise relavant lists/variables
    stations_river = stations_by_river(stations)

    #Go through all rivers and check if station.river is same as river name
    for river in stations_river:
        for station in stations_river[river]:
            assert(station.river == river)  

    print("Test stations by river: Succesful")

#Task 1E

def test_rivers_by_station_number():
    #Initialise relavant lists/variables
    N = 10
    stations_river = stations_by_river(stations)
    river_numbers = rivers_by_station_number(stations, N)
    
    #Check length of list is N
    assert(len(river_numbers) == N)

    #Check Number of Stations is also correct
    for river in river_numbers:
        assert(river[1] == len(stations_river[river[0]]))
    
    print("Test river by station number: Succesful")
    
test_stations_by_distance()
test_stations_within_radius()
test_river_with_station()
test_stations_by_river()
test_rivers_by_station_number()