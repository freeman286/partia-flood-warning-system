"""This module contains a collection of functions related to plotting.

"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit

def poly_water_level(station, dates, levels, p):
    try: # Sometimes a polynomial will not fit the data
        datesN = matplotlib.dates.date2num(dates)
        polyF,dt = polyfit(datesN-datesN[0],levels,p)
        return polyF, datesN
    except:
        return np.poly1d([0] * p), datesN

def plot_water_levels(station, dates, levels, p = 0):
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

    if (p > 0):
        # Finding curve of best fit
        polyF, datesN = poly_water_level(station, dates, levels, p)

    	# Plot curve of best fit
        plt.plot(dates, polyF(datesN-datesN[0]),'r',linewidth=1,label='Curve fit')

    # Plot main water level
    plt.plot(dates, levels,label='Data plot')

	# Plotting Typical High/Low values
    if station.typical_range_consistent():
        label_x = sorted(dates)[-1] + (sorted(dates)[-1] - sorted(dates)[0]) * 0.1
        plt.axhline(y=station.typical_range[0],c='y',linestyle='--')
        plt.text(label_x,station.typical_range[0],'typical low')
        plt.axhline(y=station.typical_range[1],c='y',linestyle='--')
        plt.text(label_x,station.typical_range[1],'typical high')

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.legend()
    plt.show()
