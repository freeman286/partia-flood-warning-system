"""This module contains a collection of functions related to
flood data.

"""

from floodsystem.station import consistent_typical_relative_water_level, relative_water_level_to_other
from floodsystem.plot import poly_water_level
from floodsystem.datafetcher import fetch_measure_levels
import datetime
import numpy as np
import matplotlib


def stations_level_over_threshold(stations, tol):
    stations = consistent_typical_relative_water_level(stations)
    data = map(lambda x: (x, x.relative_water_level()), stations)
    return list(sorted(filter(lambda x: x[1] > tol, data), key=lambda x: x[1], reverse=True))

def stations_highest_rel_level(stations, N):
    data = consistent_typical_relative_water_level(stations)
    data = map(lambda x: (x, x.relative_water_level()), data)
    return list(map(lambda x: x[0], sorted(data, key=lambda x: x[1], reverse=True)[:N]))

def stations_risk_level(stations, dt, df, resolution):
    data = []
    for station in stations:
        try:
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            polyF, datesN = poly_water_level(station, dates, levels, 5)
            if polyF != np.poly1d([0] * 5):
                levels = []
                for x in range(1, df*(1440 // resolution)): # look forward df days
                    levels.append(np.polyval(polyF, matplotlib.dates.date2num(datetime.date.today() + datetime.timedelta(minutes=resolution)*x)-datesN[0]))
                data.append([station, max(levels)])
        except:
            pass
    return data

def town_risk_level(stations, dt, df, resolution):
    data = stations_risk_level(stations, dt, df, resolution)
    towns = {}
    for d in data:
        if d[0].town in towns and towns[d[0].town] < d[1]:
            towns[d[0].town] = d[1]
        else:
            towns[d[0].town] = d[1]

    for k, v in towns.items():
        if v > 1.0:
            towns[k] = "severe"
        elif  v > 0.8:
            towns[k] = "high"
        elif v > 0.6:
            towns[k] = "moderate"
        else:
            towns[k] = "low"

    return towns
