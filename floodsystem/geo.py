# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine

def stations_by_distance(stations, p):
    data = []
    for station in stations:
        data.append((station, haversine(station.coord, p)))
    return sorted(data, key=lambda x: x[1])

def stations_within_radius(stations, p, r):
    data = stations_by_distance(stations, p)
    return list(map(lambda x: x[0], filter(lambda x: x[1] <= r, data))) # Return only stations

def rivers_with_station(stations):
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return sorted(rivers)

def stations_by_river(stations):
    data = {}
    for station in stations:
        if station.river in data:
            data[station.river].append(station)
        else:
            data[station.river] = [station]
    return data

def rivers_by_station_number(stations, N):
    data = stations_by_river(stations)
    data = sorted({k:len(v) for (k,v) in data.items()}.items(), key=lambda x: x[1], reverse=True) # Make dictionary of rivers and their number of stations (sorted descending)
    return list({k: v for k, v in data if v >= data[N-1][1]}.items()) # Return the N rivers with the greatest number of monitoring stations (allowing for draws with the Nth river)
