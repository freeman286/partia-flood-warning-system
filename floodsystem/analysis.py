import numpy as np
import matplotlib

def polyfit(dates,levels,p):
    return (np.poly1d(np.polyfit(dates,levels,p)),max(dates)-min(dates))
