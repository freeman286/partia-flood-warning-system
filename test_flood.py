# Initilise testing (Importing modules + building station list)
from floodsystem.flood import stations_highest_rel_level,stations_level_over_threshold
from floodsystem.station import *
from floodsystem.stationdata import build_station_list,update_water_levels
stations = build_station_list()



#Tests
def test_stations_level_over_threshold():
    tol = 0.8
    station_over_tol = stations_level_over_threshold(stations,tol)
    update_water_levels(stations)
	
    for station in stations:
        if type(station.relative_water_level)=='float':
            if station in station_over_tol:
                assert station.relative_water_level()>=tol
            else:
                assert station.relative_water_level()<=tol
    print("Test stations_level_over_threshold succesful")

def test_stations_highest_rel_level():
    N = 10
    station_high_level = stations_highest_rel_level(stations,N)
    update_water_levels(stations)
    #Checking for correct length,order and values for levels 
    assert len(station_high_level)==N
    for i in range(N-1):
        assert station_high_level[i+1].relative_water_level()<=station_high_level[i].relative_water_level()
    #Checking if station_high_level is highest N values
    for station in stations:
        if station not in station_high_level and type(station.relative_water_level)=='float':
            assert station.relative_water_level()<=station_high_level[N-1].relative_water_level()
    print("Test stations_highest_rel_level succesful")

test_stations_level_over_threshold()
test_stations_highest_rel_level()