# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.utils import stations_to_names

def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()

    data = inconsistent_typical_range_stations(stations)

    # Print the stations with inconsistent data
    print(stations_to_names(data))


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
